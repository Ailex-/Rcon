# A simple way to use RCon in C++
### How to add it in your project ?

- Clone the repository and copy the content in your project folder
- Include `Rcon.h` in your c++ file

### How to use ?

The first thing to do is to create an instance of the `RCon` class

```c++
#include "Path_to_rcon/Rcon.h"

    Rcon R;

```

Then you need to connect to set up the parameters for the connection, it can be done either
from the constructor or with the provided setters functions

```c++
#include "Path_to_rcon/Rcon.h"

    Rcon R(char* ServerIP,int ServerPort, int LocalPort,char* Password,char* Header);

    // or

    R.SetIP(char* ServerIP);
    R.SetServerPort(int ServerPort);
    R.SetLocalPort(int LocalPort);
    R.SetPassword(char* Password);

```
---
##### Header and local port
Different programs have slightly different implementations of rcon, this librbary provides
some function to make sure that a wide ranges of programs can bbe supported (eg. CoD4, Jka etc..)
When no header is specified the program will use the following string `char* \xff\xff\xff\xff`
from the `void Rcon::SetDefaultHeader()` function.

If no local port is specified the program will use random port given by the OS.
Ports beblow 1024 are forbidden to use.

---

After you connected to the server you can send commands and recive data

```c++
    R.SendCommmand("say Hello World!");


    char* Buffer[2048];
    R.ReciveOutput(Buffer,2048);

```

Remember to close the connection when you're done!
```c++
    R.Disconnect();
```

### Public functions list

| General |
|----------|
|Rcon(***char\*** IP=NULL, **int** ServerPort=0, **int** LocalPort=0,* ***char\*** Password=NULL, **char\*** Header=NULL*)|
|**void**  Connect()|
|**void**  SendCommand(***char\*** Command*)|
|**void**  ReciveOutput(***char\*** Buffer, int BufSize*)|
|**void**  Disconnect()|

| Getters |
|----------|
|**char*** GetIP()|
|**char*** GetHeader()|
|**int**   GetServerPort()|
|**int**   GetLocalPort()|

| Setters |
|----------|
|**void**  SetIP(***char\*** IP*)|
|**void**  SetServerPort(***int** Port*)|
|**void**  SetLocalPort(***int** Port*)|
|**void**  SetPassword(***char\*** NewPassword = NULL*)|
|**void**  SetDefaultHeader() *-> \xFF\xFF\xFF\xFF*|
|**void**  SetCustomHeader(***char\*** Header*)|

---

### C compatibility !
While this library is made for C++, the code is intentionally made to be C like to
facilitate any attempts to port the code to pure C.

### Supported platforms
The library will detect and use the correct files for the correct OS, windows specific code will
not be compiled under linux and linux specific code will not bbe compiled under windows, even if
the source files are present.

- Windows
- Linux


