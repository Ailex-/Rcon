#include "Rcon.h"

Rcon::Rcon(char* IP, int ServerPort, int LocalPort,  char* Password, char* CustomHeader){
	
	SetIP(IP);
	SetServerPort(ServerPort);
	SetLocalPort(LocalPort);

	this->Password = Password;
	
	if(CustomHeader != NULL)
		SetCustomHeader(CustomHeader);
	else
		SetDefaultHeader();
}


char* Rcon::GetIP()     	{ return this->IP    	  ; }
char* Rcon::GetHeader() 	{ return this->Header	  ; }
int   Rcon::GetLocalPort()  { return this->LocalPort  ; }
int   Rcon::GetServerPort() { return this->ServerPort ; }

void Rcon::SetIP(char* IP) {
	if(ValidateIP(IP))
		this->IP = IP;
}

void Rcon::SetServerPort(int Port) {
	if(ValidateServerPort(Port))
		this->ServerPort = Port;
}
void Rcon::SetLocalPort(int Port) {
	if(ValidateLocalPort(Port))
		this->LocalPort = Port;
}

void Rcon::SetPassword(char* Password) {
	this->Password = Password;
}

void Rcon::SetDefaultHeader(){
	this->Header = {(char*)"\xff\xff\xff\xff"};
}

void Rcon::SetCustomHeader(char* Header){
	this->Header = Header;
}
