#ifdef _WIN32
#include "../Rcon.h"


void Rcon::Connect(){
	
	// Check if there are all the parameters needed
	if(IP == NULL || ServerPort == 0){
		printf("\nError invalid IP or server port\n");
		return;
	}
	
	// Initialising WinSock
	WSADATA WinSock;
	int rc = WSAStartup(MAKEWORD(2,2), &WinSock);
	if(rc < 0 ){
		printf("\nError initializing WinSockn -- WinSock error : %d", WSAGetLastError());
		return;
	}
	
	// Remote address
	memset((char*)&RemoteAddress, '\0', sizeof(RemoteAddress));
	
	RemoteAddress.sin_family            = AF_INET           ;
	RemoteAddress.sin_port              = htons(ServerPort) ;
	RemoteAddress.sin_addr.S_un.S_addr  = inet_addr(IP)     ;
	
	
	// Server socket
	ServerSocket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(ServerSocket < 0){
		printf("\nError creating the socket -- WinSock error : %d", WSAGetLastError());
		return;
	}
	
	// Local address, used to forward the connection at the same local port
	struct sockaddr_in  LocalAddress ;
	memset((char*)&LocalAddress,'\0',sizeof(LocalAddress));
	
	LocalAddress.sin_family      = AF_INET          ;
	LocalAddress.sin_port        = htons(LocalPort) ;
	LocalAddress.sin_addr.s_addr = INADDR_ANY       ;
	
	// Bind the local address & local port to the socket
	rc = bind(ServerSocket, (struct sockaddr*)&LocalAddress, sizeof(LocalAddress));
	if (rc < 0)
		printf("\nError binding the socket to the local address -- WinSock error : %d", WSAGetLastError());
	
}

#endif
