#ifdef _WIN32
#include "../Rcon.h"


void Rcon::ReciveOutput(char* Buffer, int BufSize){
	
	if(BufSize <= 0){
		printf("\nInvalid buffer size given.\n");
		return;
	}
	
	memset(Buffer, '\0', BufSize);
	
	int AddrSize = sizeof(RemoteAddress);
	int bytes = 0;
	
	bytes = recvfrom(ServerSocket, Buffer, BufSize, 0, (struct sockaddr*)&RemoteAddress, &AddrSize);
	if(bytes < 0)
		printf("\nError reciving from the server -- WinSock error : %d", WSAGetLastError());
}

#endif
