#ifndef QUAKE_3_RCON_WRAPPER_H
#define QUAKE_3_RCON_WRAPPER_H

#include <cstdio>  // printf()
#include <cstddef> // NULL
#include <cstring> // strlen()
#include <ctype.h> // isdigit()
#include <cmath>   // pow()

#ifdef linux
	#include <arpa/inet.h>
	#include <sys/socket.h>
	#include <unistd.h>    // close()
#endif

#ifdef _WIN32
	#include <winsock2.h>
	#pragma comment(lib,"ws2_32.lib") //if you wanna use the Visual C++ compiler 
#endif

class Rcon {
	
	public:
		Rcon(char* IP = NULL, int ServerPort = 0, int LocalPort = 0, char* Pass = NULL, char* CustomHeader = NULL);
		
		char* GetIP();
		void  SetIP(char* IP);
		
		int   GetServerPort();
		void  SetServerPort(int Port);
		
		int   GetLocalPort();
		void  SetLocalPort(int Port);
		
		void  SetPassword(char* NewPassword = NULL);
		
		char* GetHeader();
		void  SetDefaultHeader();
		void  SetCustomHeader(char* Header);
		
		void  Connect();
		void  SendCommand(char* Command);
		void  ReciveOutput(char* Buffer, int BufSize);

		void  Disconnect();

	private:
		char* IP         = NULL ;
		char* Password   = NULL ;
		char* Header     = NULL ;
		int   ServerPort = 0;
		int   LocalPort  = 0;
		
		bool ValidateIP(char* IP);
		bool ValidateServerPort(int Port);
		bool ValidateLocalPort(int Port);

		int ServerSocket;

		struct sockaddr_in  RemoteAddress ;
		
};
#endif // ! QUAKE_3_RCON_WRAPPER_H
