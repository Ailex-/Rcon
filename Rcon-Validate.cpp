#include "Rcon.h"

// Functions and Libs used 
//
// <cstring>  strlen()
// <ctype.h>  isdigit()
// <cmath>    pow()


bool Rcon::ValidateIP(char* IP){
	
	if(strcmp(IP,(char*)"localhost") == 0)
		return true;

	const int IP_Size = strlen(IP);
	if(IP_Size > 6 && IP_Size < 16){
		
		int DotCount = 0;
		int NumberCount = 0;

		// Check if the given IP has the right format
		// (4 numbers, 3 dots, max number lenght = 3)
		for(int i = 0; i<IP_Size; i++){
			
			if(NumberCount > 3)
				return false;
			
			else if(IP[i] == '.'){
				DotCount = DotCount + 1;
				NumberCount = 0;
				continue;
			}
			
			else if(!isdigit(IP[i]))
				return false;
			NumberCount = NumberCount + 1;
		}
		
		if(DotCount != 3)
			return false;
		
		
		int Counter = 0;
		int Number  = 0;
		
		// Check that the number value is less
		// than 255
		for(int i = IP_Size-1; i>=0; i--){
			if (IP[i] == '.'){
				Counter = 0;
				Number = 0;
				continue;
			}
			// -48 Because of ASCII value 
			Number = Number + ((IP[i]-48) * pow(10,Counter));
			Counter = Counter + 1;
			
			if(Number > 255)
				return false;
		}
		return true;
	}
	return false; // The given char* was not the right size
}

bool Rcon::ValidateServerPort(int Port){
	return (Port > 1024 && Port <= 65535);
}

bool Rcon::ValidateLocalPort(int Port){
	return ((Port > 1024 && Port <= 65535) || Port == 0);
}
