#ifdef linux
#include "../Rcon.h"


void Rcon::SendCommand(char* Command){
	
	const int Size = 6 + strlen(Header) + strlen(Password) + strlen(Command);
	
	char* Message = new char[Size];
	memset(Message, '\0', Size);
	strcat(Message, Header        );
	strcat(Message, (char*)"rcon ");
	strcat(Message, Password      );
	strcat(Message, (char*)" "    );
	strcat(Message, Command       );
	
	
	int rc = sendto(ServerSocket, Message, Size, 0, (const struct sockaddr*)&RemoteAddress, sizeof(RemoteAddress));
	if(rc < 0)
		printf("\nError sending to the server\n");
	
}

#endif
