#ifdef linux
#include "../Rcon.h"


void Rcon::ReciveOutput(char* Buffer, int BufSize){
	
	if(BufSize <= 0){
		printf("\nInvalid buffer size given.\n");
		return;
	}
	
	memset(Buffer, '\0', BufSize);
	
	const int AddrSize = sizeof(RemoteAddress);
	int bytes = 0;
	
	bytes = recvfrom(ServerSocket, Buffer, BufSize, 0, (struct sockaddr*)&RemoteAddress, (socklen_t*)&AddrSize);
	if(bytes < 0)
		printf("\nError reciving from the server\n");
	
	if(bytes == BufSize)
		printf("\nWarning : the given buffer has been fully filled, it is possible that some data has been lost becasue of a small buffer\n");
	
}

#endif
