#ifdef linux
#include "../Rcon.h"


void Rcon::Connect(){
	
	// Check if there are all the parameters needed
	if(IP == NULL || ServerPort == 0){
		printf("\nError invalid IP or server port\n");
		return;
	}
	
	// Remote address
	memset((char*)&RemoteAddress,'\0',sizeof(RemoteAddress));
	
	RemoteAddress.sin_family      = AF_INET           ;
	RemoteAddress.sin_port        = htons(ServerPort) ;
	RemoteAddress.sin_addr.s_addr = inet_addr(IP)     ;
	
	
	// Server socket
	ServerSocket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(ServerSocket < 0){
		printf("\nError creating the socket\n");
		return;
	}
	
	// Local address, used to forward the connection at the same local port
	struct sockaddr_in  LocalAddress  ;
	memset((char*)&LocalAddress,'\0',sizeof(LocalAddress));
	
		  LocalAddress.sin_family      = AF_INET          ;
		  LocalAddress.sin_port        = htons(LocalPort) ;
		  LocalAddress.sin_addr.s_addr = INADDR_ANY       ;
	
	// Bind the local address & local port to the socket
	int rc = bind(ServerSocket, (struct sockaddr*)&LocalAddress, sizeof(LocalAddress));
	if (rc < 0)
		printf("\nError binding the socket to the local address\n");
	
}

#endif
